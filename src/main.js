import { createApp } from "vue"
import App from "./App.vue"

import { Quasar } from "quasar"
import "quasar/dist/quasar.css"
import "@quasar/extras/mdi-v6/mdi-v6.css"
import "uno.css"

import "/src/style/global.scss";

import { createRouter, createWebHistory } from "vue-router"
import { setupLayouts } from "virtual:generated-layouts"
import generatedRoutes from "virtual:generated-pages"

import { createI18n } from "vue-i18n"
import messages from "@intlify/vite-plugin-vue-i18n/messages"


const routes = setupLayouts(generatedRoutes)
const router = createRouter({
  history: createWebHistory(),
  routes,
})

const i18n = createI18n({
  locale: "pt-BR", // set locale
  fallbackLocale: "en",// set fallback locale
  globalInjection: true,
  messages
})

createApp(App).use(i18n).use(router).use(Quasar).mount("#app")
