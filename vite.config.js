import path from "path";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import Pages from "vite-plugin-pages";
import Components from "unplugin-vue-components/vite";
import Layouts from "vite-plugin-vue-layouts";
import vueI18n from "@intlify/vite-plugin-vue-i18n";
import Unocss from "unocss/vite";
import AutoImport from "unplugin-auto-import/vite";
import { quasar, transformAssetUrls } from "@quasar/vite-plugin";
import transformerDirective from "@unocss/transformer-directives";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({ template: { transformAssetUrls } }),
    Pages({ extensions: ["vue"] }),
    AutoImport({
      include: [
        /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
        /\.vue$/,
        /\.vue\?vue/, // .vue
        /\.md$/, // .md
      ],
      imports: [
        "vue",
        "vue-i18n",
        "quasar",
        { moment: [["default", "moment"]] },
        { lodash: ["_"] },
        { howler: ["Howl", "Howler"] },
      ],
    }),
    Components(),
    Layouts(),
    vueI18n({ include: path.resolve("./src/locales/**") }),
    quasar(),
    Unocss({
      transformers: [transformerDirective()],
      preprocess(matcher) {
        return matcher.startsWith("u-") ? matcher.slice(2) : undefined; // ignore
      },
    }),
  ],
});
